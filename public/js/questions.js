
    let arrayOptions = [];
    let arrayErrores = [];

    function setOption(){
        $('#listOptions').html('');
        
        arrayOptions.map((e, key)=>{
        $('#listOptions').append('<li>'+e+'</li>');
        });
    }

    /** Insert options to list */
    $("#insertOption").click(function(){
        
        let value = $("#optionInput").val();
        if(value == ""){
            alert("The option field is required");
        }else{
            arrayOptions.push(value);
            setOption();        
            $("#optionInput").val('');
        }
    });

    /** Validate form */
    $("#btn-addQuestion").click(function(e){
        console.log("validation");
        let error = false;
        if($("#input-question").val() == ""){
            arrayErrores.push("The question field is required");
            error = true;
        }
        if(arrayOptions.length == 0){
            arrayErrores.push("The options field is required");
            error = true;
        }

        if(error){
            $('#listErrors').html('');
            arrayErrores.map((e, key)=>{
                $('#listErrors').append('<li>'+e+'</li>');
            });

            $("#div-error").removeClass("d-none");
            arrayErrores = [];
        }else{
            $('#listErrors').html('');
            $("#div-error").addClass("d-none");

            saveQuestion();
        }
    });

    function saveQuestion(){
        console.log("saveQuestion");
        
        //we will send data and recive data fom our AjaxController
        let route = "{{ route('question.store') }}";
     
        $.ajax({
            type:'POST',
            url: "/question/store",
            data: {
                "_token": $('#token').val(),
                question: $("#input-question").val(),
                description: $("#input-description").val(),
                options: arrayOptions,
                test_id: $("#test_id").val()
            },
            success: function(result){
                console.log(result);
                window.location.reload()
            }
        });

        /** Test assigned */
        function saveAssign($id){
            alert("xd");

        }
    
    }