@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Test Management</h2>
        </div>
        @can('role-list')
            <div class="pull-right">
                <a class="btn btn-success mb-2" href="{{ route('tests.create') }}"> Create New Test</a>
            </div>
        @endcan
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Test</th>
        <th>Description</th>
        <th width="35%">Action</th>
    </tr>
@foreach ($data as $key => $test)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $test->title }}</td>
        <td>{{ $test->description }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('tests.questions', ['id' => $test->id]) }}">Questions</a>
            <a class="btn btn-primary" href="{{ route('tests.edit', ['id' => $test->id]) }}">Edit</a>
            <a class="btn btn-primary" href="{{ route('tests.showassign', ['id' => $test->id]) }}">assign</a>
                {!! Form::open(['method' => 'DELETE','route' => ['tests.destroy', ['id' => $test->id]],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
        </td>
    </tr>
@endforeach
</table>

{!! $data->render() !!}
@endsection