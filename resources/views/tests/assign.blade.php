@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Assign Test</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('tests.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif


{!! Form::model($test, ['method' => 'PATCH','route' => ['tests.assign', $test->id]]) !!}
<div class="row">    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Users:</strong>
            <br/>
            @foreach($users as $value)
                <label>{{ Form::checkbox('users[]', $value->id, in_array($value->id, $user_assigned) ? true : false, array('class' => 'name')) }}
                {{ $value->name }}</label>
            <br/>
            @endforeach
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}


@endsection