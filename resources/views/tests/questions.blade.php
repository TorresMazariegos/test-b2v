@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Questions {{ $test->title }}</h2>
            <input type="hidden" id="test_id" name="name" class="form-control" value=" {{ $test->id }}">
        </div>
    
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div id="div-error" class="alert alert-danger d-none">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul id="listErrors"></ul>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <strong>Question:*</strong>
                            <input type="text" id="input-question" name="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group mt-2">
                            <strong>Description: (opcional)</strong>
                            <textarea class="form-control" id="input-description" style="height:100px" name="detail" placeholder="Detail"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="row">
                            <div class="form-group col-md-8">
                                <strong>Opciones:*</strong>
                                <input type="text" id="optionInput" name="name"  class="form-control" placeholder="Option" value="prueba333">
                            </div>
                            <div class="col-md-1">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <button class="btn btn-primary mt-4" id="insertOption">Agregar</button>
                            </div>
                            <div class="col-md-12 mt-3">
                                <ul id="listOptions"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <form action="{{ route('upload.questions') }}" method="POST" enctype="multipart/form-data" style="    border: 1px solid #958c8c;
                        padding: 16px;">
                            @csrf
                              <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                   <div class="custom-file text-left">
                                       <input type="file" name="file" class="custom-file-input" id="customFile">
                                       <label class="custom-file-label" for="customFile">Choose file</label>
                                   </div>
                             </div>
                                    <button class="btn btn-primary">Import data</button>
                                   <a class="btn btn-success" download="import_questions.csv" href="../../docs/import_questions.csv">Export data</a>
                           </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" id="btn-addQuestion" class="btn btn-success mt-2 mb-4">Add question</button>
            </div>
        </div>        
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Question</th>
        <th>Description</th>
        <th width="35%">Action</th>
    </tr>
@foreach ($data as $key => $question)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $question->question }}</td>
        <td>{{ $question->description }}</td>
        <td>
            <a class="btn btn-primary" href="{{ route('tests.edit', ['id' => $question->id]) }}">Edit</a>
            
                {!! Form::open(['method' => 'DELETE','route' => ['question.destroy', $question->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
        </td>
    </tr>
@endforeach
</table>

{!! $data->render() !!}
@endsection


