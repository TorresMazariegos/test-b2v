<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Test;
use App\Models\Question;
use App\Models\Question_option;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Test::create([
            'title' => "Laravel",
            'description' => null,
            'status' => 1,
            'usr_creo' => 1,
        ]);

        Question::create([
            'question' => "¿Qué es Laravel?",
            'description' => null,
            'status' => 1,
            'usr_creo' => 1,
            'test_id' => 1,
        ]);        

        Question_option::create([
            'option' => "a",
            'correct' => 0,
            'status' => 1,
            'question_id' => 1,
            'usr_creo' => 1
        ]);

        Question_option::create([
            'option' => "b",
            'correct' => 0,
            'status' => 1,
            'question_id' => 1,
            'usr_creo' => 1
        ]);

        Question_option::create([
            'option' => "c",
            'correct' => 1,
            'status' => 1,
            'question_id' => 1,
            'usr_creo' => 1
        ]);

        Question::create([
            'question' => "¿Cuál es la última versión de Laravel?",
            'description' => null,
            'status' => 1,
            'usr_creo' => 1,
            'test_id' => 1,
        ]);

        Question_option::create([
            'option' => "a1",
            'correct' => 1,
            'status' => 1,
            'question_id' => 2,
            'usr_creo' => 1
        ]);

        Question_option::create([
            'option' => "b2",
            'correct' => 0,
            'status' => 1,
            'question_id' => 2,
            'usr_creo' => 1
        ]);

        Question_option::create([
            'option' => "c3",
            'correct' => 0,
            'status' => 1,
            'question_id' => 2,
            'usr_creo' => 1
        ]);
    }
}
