<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Test_assigned;


class TestAssignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Test_assigned::create([
            'user_id' => 2,
            'test_id' => 1
        ]);
    }
}
