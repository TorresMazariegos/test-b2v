<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_options', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->string('option');
            $table->boolean('correct')->default(0);
            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('usr_creo');

            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('usr_creo')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_options');
    }
}
