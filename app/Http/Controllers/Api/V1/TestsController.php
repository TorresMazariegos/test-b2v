<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Test;
use App\Models\User;
use App\Models\Question;
use App\Models\Question_option;

class TestsController extends Controller
{
    /**
     * Display the List test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function testList()
    {
        $test = Test::all();
        if (is_null($test)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([$test]);
    }

    public function getUser(Request $request)
    {
        $user = User::where('id', $request->id)->get();
        if (is_null($user)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([$user]);
    }

    public function questionToTest(Request $request)
    {
        $ques = Question::where('test_id', $request->test_id)->get();
        if (is_null($ques)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([$ques]);
    }

    public function optionsQuestion(Request $request)
    {
        $ops = Question_option::where('question_id', $request->question_id)->get();
        if (is_null($ops)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([$ops]);
    }
}
