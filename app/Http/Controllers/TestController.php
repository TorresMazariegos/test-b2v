<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Test;
use App\Models\Question;
use App\Models\Test_assigned;
use App\Models\Question_option;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Imports\QuestionsImport;
use Maatwebsite\Excel\Facades\Excel;

class TestController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:test-list|test-create|test-edit|test-delete', ['only' => ['index','show']]);
        $this->middleware('permission:test-create', ['only' => ['create','store', 'questions']]);
        $this->middleware('permission:test-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:test-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( Auth::user()->hasRole('Admin') ){
            $data = Test::orderBy('id','DESC')->paginate(5);
            return view('tests.index',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
        }else{
            $id_user = Auth::id();

            $data = Test::join('tests_assigned', 'tests.id', 'tests_assigned.test_id')
            ->where('tests_assigned.user_id', $id_user)
            ->select('tests.*')
            ->orderBy('id','DESC')->paginate(5);
            return view('tests.index',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
   
        Test::create([
            'title' => $request->name,
            'description' => $request->detail,
            'status' => 1,
            'usr_creo' => 1
        ]);
    
        return redirect()->route('tests.index')
                        ->with('success','Test created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $tests
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        return view('tests.show',compact('test'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::find($id);
        return view('tests.edit',compact('test'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
    
        $test = Test::find($id);

        $test->title        = $request->name;
        $test->description  = $request->detail;
        $test->save();
    
        return redirect()->route('tests.index')
                        ->with('success','Test updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $Test
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = Test::find($id);
        $test->delete();
    
        return redirect()->route('tests.index')
            ->with('success','Test deleted successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showassign($id)
    {
        $test = Test::find($id);
        $users = User::get();
        $user_assigned = DB::table("tests_assigned")->where("tests_assigned.test_id",$id)
            ->pluck('tests_assigned.user_id','tests_assigned.user_id')
            ->all();
    
        return view('tests.assign',compact('test','users','user_assigned'));
    }

    /**
     * Assign Test to User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assign(Request $request, $id)
    {

        request()->validate([
            'users' => 'required'
        ]);
        $assigned = Test_assigned::where("test_id", $id);
        $assigned->delete();

        $test_a = $request->users; //Array options

        foreach($test_a as $ep=>$det)
        {                
            Test_assigned::create([
                'user_id' => $det,
                'test_id' => $id
            ]);
        }
    
        $test = Test::find($id);
        $users = User::get();
        $user_assigned = DB::table("tests_assigned")->where("tests_assigned.test_id",$id)
            ->pluck('tests_assigned.user_id','tests_assigned.user_id')
            ->all();
    
        return view('tests.assign',compact('test','users','user_assigned'));
        
    }

    /**
     * Display a listing of the questions .
     *
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request, $id)
    {
        $test = Test::find($id);
        $data = Question::where('test_id', $id)->orderBy('id','DESC')->paginate(5);
                
        return view('tests.questions',compact('data', 'test'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Store a newly created resource in storage - Questions
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function questionStore(Request $request)
    {
        $id_user= Auth::id();

        $ques = Question::create([
            'question' => $request->question,
            'description' => $request->description,
            'status' => 1,
            'usr_creo' => $id_user,
            'test_id' => $request->test_id,
        ]);

        $q_option = $request->options; //Array options

        foreach($q_option as $ep=>$det)
        {                
            Question_option::create([
                'option' => $det,
                'correct' => 0,
                'status' => 1,
                'question_id' => $ques->id,
                'usr_creo' => $id_user
            ]);
        }
        
        return $request->test_id;
    
    }

    /**
     * Remove the specified resource from storage - Question.
     *
     * @param  \App\Test  $Test
     * @return \Illuminate\Http\Response
     */
    public function questionDestroy(Request $request, $id)
    {
        $quest = Question::find($id);
        $options = Question_option::where('question_id', $quest->id);
        
        $test = Test::find($quest->test_id);

        $options->delete();
        $quest->delete();
            
        $data = Question::where('test_id', $test->id)->orderBy('id','DESC')->paginate(5);
                
        return view('tests.questions',compact('data', 'test'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /** Import questions with CSV */
    public function uploadQuestions(Request $request)
    {
        
        $file = $request->file('file');
        //var_dump($file); exit;
        //Excel::import(new QuestionsImport, $request->file);
        Excel::import(new QuestionsImport, $file);
        return back();

        //return redirect()->route('tests.index')->with('success', 'User Imported Successfully');
    }
}
