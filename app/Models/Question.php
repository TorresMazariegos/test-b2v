<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'question', 'description', 'status', 'test_id', 'usr_creo'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function test(){
        return $this->belongsTo(Test::class);
    }

    public function options(){
        return $this->hasMany(Question_option::class);
    }
}
