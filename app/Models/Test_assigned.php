<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test_assigned extends Model
{
    use HasFactory;

    protected $table = 'tests_assigned';

    protected $fillable = [
        'user_id', 'test_id'
    ];
}
