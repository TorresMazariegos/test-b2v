<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TestController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    
    Route::get('/tests/index', [TestController::class, 'index'])->name('tests.index');
    Route::get('/tests/create', [TestController::class, 'create'])->name('tests.create');
    Route::post('/tests/store', [TestController::class, 'store'])->name('tests.store');
    Route::get('/tests/{id}/edit', [TestController::class, 'edit'])->name('tests.edit');
    Route::put('/tests/{id}/update', [TestController::class, 'update'])->name('tests.update');
    Route::delete('/tests/{id}/destroy', [TestController::class, 'destroy'])->name('tests.destroy');
    Route::get('/tests/{id}/questions', [TestController::class, 'questions'])->name('tests.questions');
    Route::get('/tests/{id}/showassign', [TestController::class, 'showassign'])->name('tests.showassign');
    Route::patch('/tests/{id}/assign', [TestController::class, 'assign'])->name('tests.assign');
    
    Route::post('/question/store', [TestController::class, 'questionStore'])->name('question.store');
    Route::delete('/question/{id}/destroy', [TestController::class, 'questionDestroy'])->name('question.destroy');
    
    Route::post('/tests/import-questions', [TestController::class, 'uploadQuestions'])->name('upload.questions');
});