<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/tests/list', [App\Http\Controllers\Api\V1\TestsController::class, 'testList']);

Route::get('/user', [App\Http\Controllers\Api\V1\TestsController::class, 'getUser']);

Route::get('/test/question', [App\Http\Controllers\Api\V1\TestsController::class, 'questionToTest']);

Route::get('/question/options', [App\Http\Controllers\Api\V1\TestsController::class, 'optionsQuestion']);
